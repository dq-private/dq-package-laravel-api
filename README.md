# Easy API calls in Laravel

This package makes interacting with apis extremely simple.

- REST
- GraphQL

## Installation

First, install Api via the Composer package manager:

```bash
composer require dolphiq/api
```

After installing the package, you should publish the configuration using the `vendor:publish` Artisan command. This command will publish the `api-client.php` configuration file to your config directory:

```bash
php artisan vendor:publish --provider="Dolphiq\Api\ApiServiceProvider"
```

Now you can use the package in your Laravel application:
```php
Api::get('/your-url-here', ['query'=>[]]);
Api::post('/your-url-here', ['form_params'=>[]]);
Api::put('/your-url-here', ['body'=>[]]);
Api::patch('/your-url-here', ['body'=>[]]);
Api::delete('/your-url-here', ['body'=>[]]);
```

When using a second config use
```php
Api::api('cms-graph-config')->get('/your-url-here', ['query'=>[]]);
```

## Configuration

### Choose the driver
The package can create REST and GraphQL api calls.

```bash
'driver' => 'rest'
'driver' => 'graphql'
```

### Configuring driver
The package uses guzzle to create the http requests. You can configure all guzzle options ([http://docs.guzzlephp.org/en/stable/request-options.html]):
```bash
'request_options' => []
```

### Configuring another api connection
Add another api connection in `api-client.php`:
```bash
'another_api' => [
    'driver' => 'rest',
    'debug' => true,
    'request_options' => [
        'base_uri' => '',
        'connect_timeout' => 2.0,
    ],
]
```

Use the api connection
```php
    Api::api('another_api')->getJson('/your-url-here', ['query-as-array'=>'here']);
```

## Retrieving data
For all methods like `get` and `post` which retrieve results, an instance of Illuminate\Database\Eloquent\Collection will be returned.

```php
    try {
        $collection = Api::get('/your-url-here', ['query-as-array'=>'here']);
    } catch (\Exception $exception) {
        
    }
```


### Testing

```bash
composer test
```

## License
