<?php
return [

    'default' => env('API_DRIVER', 'default'),

    'apis' => [

        'default' => [
            'driver' => 'rest',
            'debug' => true,
            'request_options' => [
                'base_uri' => ''
            ],
        ],

    ]


];
