<?php

namespace Dolphiq\Api;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class ApiServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * @var array
     */
    protected $publicDrivers = [];

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/api-client.php' => config_path('api-client.php'),
            ], 'config');
        }
        
        Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                if (is_array($value)) {
                    return collect($value)->recursive();
                }
                if (is_object($value)) {
                    return collect($value)->recursive();
                }

                return $value;
            });
        });

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {

        $this->app->singleton('Dolphiq\Api', function ($app) {
            return new ApiManager($app);
        });

    }



    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [
            'Dolphiq\Api'
        ];
    }
}
