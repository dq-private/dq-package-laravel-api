<?php

namespace Dolphiq\Api;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Dolphiq\Api\Exceptions\ResponseNotSupportedException;
use Dolphiq\Api\Exceptions\ResponseException;
use Psr\Http\Message\ResponseInterface;

abstract class Api
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * Api constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function client()
    {
        return $this->client;
    }

    /**
     * @param $method
     * @param string $uri
     * @param array $options
     * @return Collection
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function request($method, string $uri = '', array $options = []): Collection
    {
        try {
            $response = $this->client->request($method, $uri, $options);
            return $this->response($response);
        } catch (\Exception $exception) {
            throw new ResponseException('Guzzle exception: ' . $exception->getMessage());
        }
    }

    /**
     * @param ResponseInterface $response
     * @return Collection
     * @throws ResponseException
     * @throws ResponseNotSupportedException
     */
    protected function response(ResponseInterface $response): Collection
    {
        $responseBody = $response->getBody()->getContents();
        if ($responseBody === false) {
            throw new ResponseException('Response from guzzle stream failure');
        }

        if ($responseBody === '') {
            return new Collection(null);
        }

        if (strpos($response->getHeaderLine('Content-Type'), 'application/json') !== false && json_decode($responseBody,
                true) !== null) {
            $responseBody = json_decode($responseBody, true);

            if (isset($responseBody['errors'])) {
                throw new ResponseException('Errors returned from api: ' . json_encode($responseBody['errors']));
            }

            $collection = new Collection($responseBody);
            return $collection->recursive();
        }

        /*
         * NOT YET SUPPORTED OR TESTED!
        if (simplexml_load_string($responseBody) !== null) {
            $collection = new Collection(simplexml_load_string($responseBody));
            return $collection->recursive();
        }*/

        throw new ResponseNotSupportedException('Expected json or xml response');
    }

}
