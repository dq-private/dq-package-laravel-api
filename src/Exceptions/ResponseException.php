<?php

namespace Dolphiq\Api\Exceptions;

use Exception;
use Log;

class ResponseException extends Exception
{

    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        Log::log('error', $this->getMessage());
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response|void
     */
    public function render($request)
    {
        return abort(404, __('Something wrong with a api call'));
    }

}
