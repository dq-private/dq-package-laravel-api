<?php

namespace Dolphiq\Api;

use Illuminate\Support\Arr;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Psr\Http\Message\ResponseInterface;
use InvalidArgumentException;
use Log;

class ApiManager
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * The active api instances.
     *
     * @var array
     */
    protected $apis = [];

    protected CONST CONFIG = [
        'debug' => false,
        'request_options' => [
            'connect_timeout' => 2.0
        ]
    ];

    /**
     * Create a new APi manager instance.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Get a api instance.
     *
     * @param string|null $name
     * @return \GuzzleHttp\Client
     */
    public function api($name = null)
    {
        $name = $name ?: $this->getDefaultApi();

        if (!isset($this->apis[$name])) {
            $this->apis[$name] = $this->makeApi($name);
        }

        return $this->apis[$name];
    }

    /**
     * Make the api instance.
     *
     * @param string $name
     * @return \GuzzleHttp\Client
     */
    protected function makeApi($name)
    {
        $config = $this->configuration($name);

        if (isset($config) && $config['debug'] === true) {
            if (!isset($config['request_options']['handler'])) {
                $config['request_options']['handler'] = HandlerStack::create();
            }

            $config['request_options']['handler']->push(Middleware::mapResponse(function (
                ResponseInterface $response
            ) {
                $response->getBody()->rewind();
                return $response;
            }));

            $config['request_options']['handler']->push(
                Middleware::log(
                    Log::getLogger(),
                    new MessageFormatter('{request} - {response}')
                )
            );
        }

        return $this->getDriver($config, new Client($config['request_options']));

    }

    /**
     * Get the configuration for a api.
     *
     * @param string $name
     * @return array
     *
     * @throws \InvalidArgumentException
     */
    protected function configuration($name)
    {
        $name = $name ?: $this->getDefaultConnection();

        $apis = $this->app['config']['api-client.apis'];

        if (is_null($config = Arr::get($apis, $name))) {
            throw new InvalidArgumentException("Api [{$name}] not configured.");
        }

        return array_merge_recursive(
            $config,
            self::CONFIG
        );
    }

    /**
     * Remove from local cache.
     *
     * @param string|null $name
     * @return void
     */
    public function purge($name = null)
    {
        $name = $name ?: $this->getDefaultApi();

        unset($this->apis[$name]);
    }

    /**
     * Get the default api name.
     *
     * @return string
     */
    public function getDefaultApi()
    {
        return $this->app['config']['api-client.default'];
    }

    /**
     * Set the default api name.
     *
     * @param string $name
     * @return void
     */
    public function setDefaultApi($name)
    {
        $this->app['config']['api-client.default'] = $name;
    }

    /**
     * Return all of the created apis.
     *
     * @return array
     */
    public function getApis()
    {
        return $this->apis;
    }

    protected function getDriver(array $config, Client $client)
    {
        $apiMethod = strtolower($config['driver']) . 'Api';

        if (method_exists($this, $apiMethod)) {
            return $this->{$apiMethod}($client);
        }

        throw new InvalidArgumentException("Api [{$config['driver']}] is not supported.");
    }

    /**
     * @param Client $client
     * @return RestApi
     */
    protected function restApi(Client $client): RestApi
    {
        return new RestApi($client);
    }

    /**
     * @param Client $client
     * @param array $config
     * @return GraphqlApi
     */
    protected function graphqlApi(Client $client): GraphqlApi
    {
        return new GraphqlApi($client);
    }

    /**
     * Dynamically pass methods to the default connection.
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return $this->api()->$method(...$parameters);
    }
}
