<?php

namespace Dolphiq\Api\Contracts;

use Illuminate\Support\Collection;

interface RestApi
{
    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     */
    public function get(string $url = '', array $requestOptions = []): Collection;

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     */
    public function post(string $url = '', array $requestOptions = []): Collection;

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     */
    public function put(string $url = '', array $requestOptions = []): Collection;

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     */
    public function patch(string $url = '', array $requestOptions = []): Collection;

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     */
    public function delete(string $url = '', array $requestOptions = []): Collection;
}
