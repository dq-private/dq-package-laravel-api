<?php

namespace Dolphiq\Api\Contracts;

use Illuminate\Support\Collection;

interface GraphqlApi
{
    public function get($query, $variables = []): Collection;
}
