<?php

namespace Dolphiq\Api;

use Dolphiq\Api\Contracts\RestApi as ApiContract;
use Illuminate\Support\Collection;

class RestApi extends Api implements ApiContract
{

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     * @throws Exceptions\ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $url = '', array $requestOptions = []): Collection
    {
        return $this->request('get', $url, $requestOptions);
    }

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     * @throws Exceptions\ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $url = '', array $requestOptions = []): Collection
    {
        return $this->request('post', $url, $requestOptions);
    }

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function put(string $url = '', array $requestOptions = []): Collection
    {
        return $this->request('put', $url, $requestOptions);
    }

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function patch(string $url = '', array $requestOptions = []): Collection
    {
        return $this->request('patch', $url, $requestOptions);
    }

    /**
     * @param string $url
     * @param array $requestOptions
     * @return Collection
     * @throws ResponseException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $url = '', array $requestOptions = []): Collection
    {
        return $this->request('delete', $url, $requestOptions);
    }

}
