<?php

namespace Dolphiq\Api;

use Dolphiq\Api\Contracts\GraphqlApi as ApiContract;
use Illuminate\Support\Collection;

class GraphqlApi extends Api implements ApiContract
{

    /**
     * @param $query
     * @param array $variables
     * @return Collection
     */
    public function get($query, $variables = []): Collection
    {
        return $this->request('post', '',[
            'body' => json_encode([
                'query' => $query,
                'variables' => $variables
            ])
        ]);
    }
}
