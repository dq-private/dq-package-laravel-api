<?php
namespace Dolphiq\Api\Tests\Traits;

trait JsonSnapshot
{
    private function snapshotsFolder()
    {
        return base_path('tests/snapshots');
    }

    public function snapshot($key, callable $fn)
    {
        $filesystem = new Filesystem($this->snapshotsFolder());
        $driver = new JsonDriver();
        $snapshot = new Snapshot($key, $filesystem, $driver);
        if ($snapshot->exists()) {
            return $filesystem->read("$key.json");
        }
        $snapshot->create(call_user_func($fn));
        return $filesystem->read("$key.json");
    }
}
