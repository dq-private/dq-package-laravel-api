<?php

namespace Dolphiq\Api\Tests\Driver\Json;

use Dolphiq\Api\Tests\TestCase;
use Dolphiq\Api\Exceptions\ResponseException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Collection;
use TypeError;

class ApiRequestGetTest extends TestCase
{
    public function newMockGetRequest($mockResponse, ...$apiParams)
    {
        $mock = new \GuzzleHttp\Handler\MockHandler($mockResponse);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        /** @var \Dolphiq\Api\Contracts\Api $api */
        //Overwrite config here!
        config(['api-client.apis.default.request_options.handler' => $handler]);
        $api = $this->app->make('Dolphiq\Api');
        return $api->getJson(...$apiParams);
    }

    public function test_it_guards_against_invalid_url_params(): void
    {
        // must be of the type string, null given
        $this->expectException(TypeError::class);

        $response = $this->newMockGetRequest([
            new Response(200, [], json_encode(['test' => 'test']))
        ], null, ['test' => 'test']);

    }

    public function test_it_guards_against_invalid_query_params(): void
    {
        // must be of the type array, string given
        $this->expectException(TypeError::class);

        $response = $this->newMockGetRequest([
            new Response(200, [], json_encode(['test' => 'test']))
        ], '/test', 'test');
    }

    public function test_it_can_get_success_result(): void
    {
        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new Response(200, [], json_encode(['test' => 'test', 'test2' => ['test'=>'test']]))
        ]);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertTrue($response->isNotEmpty());
        $this->assertTrue($response->has('test'));
        $this->assertSame($response->get('test'), 'test');

    }

    public function test_it_can_get_created_success_result(): void
    {
        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new Response(201)
        ]);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertTrue($response->isEmpty());

    }

    public function test_it_can_get_redirect_success_result(): void
    {
        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new Response(300, [], json_encode(['url' => 'test']))
        ]);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertTrue($response->isNotEmpty());
        $this->assertTrue($response->has('url'));
        $this->assertSame($response->get('url'), 'test');

    }

    public function test_it_can_get_bad_request_success_result(): void
    {
        $this->expectException(ResponseException::class);
        $this->expectExceptionMessageMatches('/Client error/i');

        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new Response(404, []),
        ]);
    }

    public function test_it_can_get_not_reachable_success_result(): void
    {
        $this->expectException(ResponseException::class);
        $this->expectExceptionMessageMatches('/Error Communicating with Server/i');

        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
        ]);
    }

    public function test_it_can_get_not_reachable_success_result1(): void
    {
        $this->expectException(ResponseException::class);
        $this->expectExceptionMessageMatches('/Error Communicating with Server/i');

        /** @var Collection $response */
        $response = $this->newMockGetRequest([
            new RequestException('Error Communicating with Server', new Request('GET', 'test'))
        ]);
    }

}
