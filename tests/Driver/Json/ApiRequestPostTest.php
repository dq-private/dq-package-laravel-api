<?php

namespace Dolphiq\Api\Tests\Driver\Json;

use Dolphiq\Api\Tests\TestCase;
use Api;
use Dolphiq\Api\Exceptions\ResponseException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use \Illuminate\Support\Collection;
use TypeError;

class ApiRequestPostTest extends TestCase
{
    /**
     * @param $mockResponse
     * @param mixed ...$apiParams
     * @return Collection
     */
    public function newMockPostRequest($mockResponse, ...$apiParams): Collection
    {
        $mock = new \GuzzleHttp\Handler\MockHandler($mockResponse);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        /** @var \Dolphiq\Api\Contracts\Api $api */
        config(['api-client.apis.default.request_options.handler' => $handler]);
        $api = $this->app->make('Dolphiq\Api');
        return $api->postJson(...$apiParams);
    }

    /** @test */
    public function it_guards_against_invalid_url_params(): void
    {
        $this->expectException(TypeError::class);

        $response = $this->newMockPostRequest([
            new Response(200, [], json_encode(['test' => 'test']))
        ], null, ['test'=>'test']);
    }

    /** @test */
    public function it_guards_against_invalid_query_params(): void
    {
        $this->expectException(TypeError::class);

        $response = $this->newMockPostRequest([
            new Response(200, [], json_encode(['test' => 'test']))
        ], '/test', 'test');
    }

    /** @test */
    public function it_can_get_success_result(): void
    {
        /** @var Collection $response */
        $response = $this->newMockPostRequest([
            new Response(200, [], json_encode(['test' => 'test']))
        ]);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertTrue($response->isNotEmpty());
        $this->assertTrue($response->has('test'));
        $this->assertSame($response->get('test'), 'test');

    }


}
