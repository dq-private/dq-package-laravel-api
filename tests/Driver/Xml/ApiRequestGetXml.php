<?php

namespace Dolphiq\Api\Tests;

use Api;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use \Illuminate\Support\Collection;

class ApiRequestGetXml extends TestCase
{
    public function newMockGetRequest($mockResponse)
    {
        $mock = new \GuzzleHttp\Handler\MockHandler($mockResponse);
        $handler = \GuzzleHttp\HandlerStack::create($mock);
        $api = $this->app->make('Dolphiq\Api', ['handler' => $handler]);

        return $api->get();
    }


    /** @test */
    public function it_can_get_success_from_xml_result()
    {
        $test_array = array (
            'bla' => 'blub',
            'foo' => 'bar',
            'another_array' => array (
                'stack' => 'overflow',
            ),
        );
        $xml = new \SimpleXMLElement('<root/>');
        array_walk_recursive($test_array, array ($xml, 'addChild'));

        $response = $this->newMockGetRequest([
            new Response(200, [], $xml->asXML())
        ]);

        $this->assertInstanceOf(Collection::class, $response);

    }
}
