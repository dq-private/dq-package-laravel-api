<?php
namespace Dolphiq\Api\Tests;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Dolphiq\Api\Tests\Traits\JsonSnapshot;

class SnapshotClient extends GuzzleClient
{
    use JsonSnapshot;
    //Overwriting the get() method
    public function get(string $path = '/', array $params = [], array $options = [])
    {
        $key = md5($path . '?' . http_build_query($params));
        $response = $this->snapshot(
            $key,
            function () use ($path, $params, $options) {
                return $this->responseToJson(
                    parent::get($path, $params, $options)
                );
            });
        return $this->jsonToResponse($response);
    }
    public function responseToJson(GuzzleResponse $response)
    {
        $data = [
            'status' => $response->getStatusCode(),
            'headers' => $response->getHeaders(),
            'body' => (string)$response->getBody()
        ];
        return (string)json_encode($data);
    }

    public function jsonToResponse($snapshot)
    {
        $response = json_decode($snapshot, true);
        return new GuzzleResponse(
            $response['status'],
            $response['headers'],
            $response['body']
        );
    }
}
