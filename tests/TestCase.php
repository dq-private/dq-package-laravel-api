<?php

namespace Dolphiq\Api\Tests;

use Orchestra\Testbench\TestCase as Orchestra;
use Dolphiq\Api\ApiServiceProvider;
use Dolphiq\Api\Facades\Api as ApiFacade;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

//        $this->setUpDatabase($this->app);
//
//        $this->withFactories(__DIR__.'/factories');
    }

    protected function getPackageProviders($app)
    {
        return [ApiServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Api' => ApiFacade::class
        ];
    }

}
